Procedure to run the Montreal_Crime_Path_Finding.ipynb


First make sure that Anaconda and python is installed in the lab machine.
If not please follow the below steps

If Python is installed, you can check the version:

$ python --version

When you open a new terminal

Open a terminal and run this command to set the Anaconda to the environment path:

$ module load anaconda3

Ensure the Numpy, Matplotlib, Geopandas and Jupyter packages are installed:

$ conda install numpy matplotlib geopandas jupyter
Installing the packages may take a few minutes.

Start a Jupyter Notebook instance
To open the notebook, run the following in a terminal at which the Montreal_Crime_Path_Finding.ipynb, Shape and Bonus folder has been placed

$ cd <directory where you placed the above mentioned files>
$ jupyter notebook


Once opened Click on Cell -> Run All.

Enter the required data when prompted. Below is an instance:
Enter the Threshold value, you should enter int value, ex 50 etc.
Enter the grid cell size, you should enter float value, ex 0.002 etc.